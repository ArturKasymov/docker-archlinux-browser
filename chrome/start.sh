docker build -t chrome_browser .
case "${unameOut}" in
    Linux*) docker run --privileged --memory 512mb --rm -ti -p 5900:5900 --device /dev/snd chrome_browser ;;
    *) docker run --privileged --rm -ti -p 5900:5900 chrome_browser ;;
esac
