#!/bin/bash
export DISPLAY=:1
Xvfb $DISPLAY -screen 0 1920x1080x16 +extension RANDR \
    >  ~/xvfb.out.log \
    2> ~/xvfb.err.log &
fluxbox -rc /fluxbox_init \
    >  ~/fluxbox.out.log \
    2> ~/fluxbox.err.log &
chromium --disable-gpu --start-maximized \
    >  ~/chromium.out.log \
    2> ~/chromium.err.log &
x11vnc -display $DISPLAY -noxrecord -noxfixes -noxdamage -forever -passwd 123456
