# docker-archlinux-browser

## Usage:
1. Install Docker on your machine: https://docs.docker.com/engine/install/

2. Make sure Docker engine is running:
    * **On linux:**
     Command `sudo systemctl status docker` should return `active (running)`.
     If its not active enter `sudo systemctl enable docker`.
    * You can always test Docker Engine with command: `docker run hello-world`. 

3. Run `./start.sh firefox`

4. Open vnc client on `localhost:5900` and password `123456`
    * For example using `vncviewer`.
     In terminal write `vncviewer localhost:5900` than enter password.  

5. (Optional) If you start docker on your virtual machine: 
    
    ** VirtualBox  **
    
    you may use port forwarding to access docker browser from your host machine:
    
    Machine -> Settings -> Network -> Advanced -> Port Forwarding -> Add new Port Forwarding rule -> Set Guest Port = 5900 and Host Port as you wish.

    ![alt text](images/virtual_box_port_forward.png)
    
    Now you may open vnc client on `localhost:5900` and password `123456` on your host and get access to your VM docker browser
    
    ** VMware Workstattion **
    
    you may connect to your VM port from host:
    
    Player -> Manage -> Virtual Machine Settings -> Network Adapter -> use any of shared network options
    
    Get ip of your VM (for example run `ifconfig`).
    
    Now you may open vnc client on `<Your VM ip address>:5900` and password `123456` on your host and get access to your VM docker browser

 
            
            

## Used materials:

* https://youtu.be/E-GwJwulyxE
* https://github.com/jessfraz/dockerfiles/blob/master/chrome/stable/Dockerfile
* http://www.karlrunge.com/x11vnc/faq.html
	
## Current issues:
* Firefox:
    * audio only on linux host
    * ~~right click~~
* Chrome:
    * works only in maximized mode
    * crashes

* Opera:
    * crashes

## How issues were solved:
* Firefox:
    * **disappearing right click menu** - Running fluxbox with virtual display solved issue. 
      Looks like firefox has unspecified behavior when it's running on bare Xvfb display.
      Another advantage of using fluxbox is that now user can see multiple windows of browser.