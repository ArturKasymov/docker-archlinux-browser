docker build -t browser-common browser-common
case $1 in
	firefox)
		(cd firefox; ./start.sh)
		;;
    chrome)
        (cd chrome; ./start.sh)
        ;;
	*)
		echo "unexpected value"
		;;
esac
