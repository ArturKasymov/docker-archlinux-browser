docker build -t opera_browser .
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*) docker run --rm -ti -p 5900:5900 --device /dev/snd --privileged opera_browser ;; # run linux with audio 
    *) docker run --rm -ti -p 5900:5900 --privileged opera_browser # no audio on other systems
esac
