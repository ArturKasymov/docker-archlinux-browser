docker build -t firefox_browser .
unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*) docker run --rm -ti -p 5900:5900 --device /dev/snd firefox_browser;; # run linux with audio 
    *) docker run --rm -ti -p 5900:5900 firefox_browser # no audio on other systems
esac
