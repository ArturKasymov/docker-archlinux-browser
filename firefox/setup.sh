#!/bin/bash

export DISPLAY=:1
Xvfb $DISPLAY -screen 0 1920x1080x16 \
    >  /xvfb.out.log \
    2> /xvfb.err.log &
fluxbox -rc /fluxbox_init \
    >  /fluxbox.out.log \
    2> /fluxbox.err.log &
firefox -width 1920 -height 1080 \
    >  /firefox.out.log \
    2> /firefox.err.log &
x11vnc -display $DISPLAY -noxrecord -noxfixes -noxdamage -forever -passwd 123456
