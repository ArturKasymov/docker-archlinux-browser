# Directory description
Docker file based on arch linux with common data for all browsers images.
Including:
* Xvbf - for virtual display
* x11vnc - for accessing container display from host machine
* fluxbox - window manager.
    * fluxbox_init - file with fluxbox's settings
* pulseaudio - for sharing audio